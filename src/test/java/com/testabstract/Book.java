package com.testabstract;

public abstract class Book {

	abstract void write();

	abstract void read();

}
