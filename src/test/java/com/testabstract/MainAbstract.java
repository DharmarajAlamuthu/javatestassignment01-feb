package com.testabstract;

public class MainAbstract extends Book {

	@Override
	void write() {
		System.out.println("The writing a book");

	}

	@Override
	void read() {
		System.out.println("The reading a book");

	}

	public static void main(String[] args) {

		MainAbstract abs = new MainAbstract();
		abs.read();
		abs.write();

	}

}
