package com.testabstract;

public class NoteBook extends Book {

	@Override
	void write() {
		System.out.println("The writing a book");

	}

	@Override
	void read() {
		System.out.println("The reading a book");

	}

	void draw() {
		System.out.println("The drawing a book");
	}

	public static void main(String[] args) {
		NoteBook book = new NoteBook();
		book.draw();
		book.read();
		book.write();
	}

}
