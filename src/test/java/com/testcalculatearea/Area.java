package com.testcalculatearea;

public class Area extends Shap {

	double Area = 0;

	@Override
	void RectangleArea(float length, float breadth) {

		Area = length * breadth;
		System.out.println("Area of rectangle is: " + Area);

	}

	@Override
	void SquareArea(float side) {

		Area = side * side;
		System.out.println("Area of Square is: " + Area);

	}

	@Override
	void CircleArea(float radius) {

		Area = (radius * radius) * 3.14;
		System.out.println("Area of Circle is: " + Area);

	}

}
