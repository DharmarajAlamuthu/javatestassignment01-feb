package com.testcalculatearea;

public class CalculateArea {

	double area = 0;

	public void rectangle(float length, float breadth) {
		area = length * breadth;
		System.out.println("Area of rectangle is: " + area);
	}

	public void square(float side) {
		area = side * side;
		System.out.println("Area of square is: " + area);
	}

	public void circle(float radius) {
		area = (radius * radius) * 3.14;
		System.out.println("Area of circle is: " + area);
	}

	public static void main(String[] args) {
		CalculateArea cal = new CalculateArea();
		cal.rectangle(5.2f, 6.3f);
		cal.square(3.1f);
		cal.circle(4.0f);
	}
}
