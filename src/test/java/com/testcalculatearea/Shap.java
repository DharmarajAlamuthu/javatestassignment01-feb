package com.testcalculatearea;

public abstract class Shap {

	abstract void RectangleArea(float length, float breadth);

	abstract void SquareArea(float radius);

	abstract void CircleArea(float side);

}
