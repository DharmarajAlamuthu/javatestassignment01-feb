package com.testcar;

public class Santro extends Car implements BasicCar {

	public void remoteStart() {

		System.out.println("This car using remotestart");

	}

	public void gearChange() {

		System.out.println("The car gearchange in 1 to 2");

	}

	public void music() {

		System.out.println("The car play music");

	}

	public static void main(String[] args) {

		Santro car = new Santro();
		car.drive();
		car.gearChange();
		car.music();
		car.remoteStart();
		car.stop();
	}

}
