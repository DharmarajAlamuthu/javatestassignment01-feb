package com.testpolymorphism;

public class Car {

	// Method Overloading

	public void typeCar() {

		System.out.println("The all car in india");

	}

	public void typeCar(String hyundai) {

		System.out.println("This car name " + hyundai);

	}

	public void typeCar(String maruti, String tata) {

		System.out.println("This car names " + maruti + " and " + tata);

	}

	public void typeCar(int car) {

		System.out.println("The car is BMW " + car);

	}

	public static void main(String[] args) {
		Car c = new Car();
		c.typeCar();
		c.typeCar(1);
		c.typeCar("Hyundai");
		c.typeCar("Maruti", "Tata");
	}

}
