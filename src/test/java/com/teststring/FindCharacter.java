package com.teststring;

public class FindCharacter {

	public static void main(String[] args) {

		String str = "Hello, World";
		int letfirst = str.indexOf("o");
		int charfirst = str.indexOf(",");
		int letlast = str.lastIndexOf("o");
		int charlast = str.lastIndexOf(",");

		System.out.println("The string find the first occurrence of the letter o is: " + letfirst);
		System.out.println("The string find the first occurrence of the character , is: " + charfirst);
		System.out.println("The string find the last occurrence of the letter o is: " + letlast);
		System.out.println("The string find the last occurrence of the character , is: " + charlast);

	}

}
